package com.mavie.bibi.text9001.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class Text9001Controller {

    @RequestMapping("/test")
    public String test(){
        return "test";
    }
}
