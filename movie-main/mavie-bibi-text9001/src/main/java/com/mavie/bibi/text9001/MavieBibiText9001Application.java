package com.mavie.bibi.text9001;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/*
 @author csy
 */
@SpringBootApplication
//@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
//@EnableDiscoveryClient
public class MavieBibiText9001Application {


    public static void main(String[] args) {
        SpringApplication.run(MavieBibiText9001Application.class,args);
    }

}
