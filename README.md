# movie-main



项目架构及流程图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/180936_9c097605_1274742.png "购买项目.png")

#0.0.1
#### 介绍
java  :  1.8 <br> 
maven :  apache-maven-3.5.2_2 <br> 
spring-cloud <br> 
SpringCloudAlibaba <br> 

nacos 1.1.4 <br> 




#### 软件架构
软件架构说明
movie-main <br> 
|__________mavie-bibi-auth  -- 公共授权服务 <br> 
|__________mavie-bibi-common  -- 公共组件 <br> 
|__________mavie-bibi-geteway  -- 公共网关 <br> 
|__________mavie-bibi-manage  -- 服务监控管理运行模块 <br> 
|__________mavie-bibi-service  -- 微服务集合 <br> 
|__________mavie-bibi-service-api  -- 微服务API集合 <br> 



#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
